var express = require('express');
var bodyParser = require('body-parser');
var awsServerlessExpressMiddleware = require('aws-serverless-express/middleware');
var cors = require('cors');
const socketIo = require('socket.io');

// declare a new express app
var app = express();

const server = require('http').Server(app);

const io = socketIo(server);
app.use(cors());

const connectedUsers = {};

io.on('connection', (socket) => {
  const { id } = socket.handshake.query;
  connectedUsers[id] = socket.id;
});

app.use((req, res, next) => {
  req.io = io;
  req.connectedUsers = connectedUsers;
  return next();
});

app.use(bodyParser.json());
app.use(awsServerlessExpressMiddleware.eventContext());

// Enable CORS for all methods
app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.get('/socket', function (req, res) {
  // Add your code here
  res.json({ success: 'get call succeed!', url: req.url });
});

app.post('/socket', async (req, res) => {
  const { idPublisher, exercise } = req.body;
  const ownerSocket = req.connectedUsers[idPublisher];

  if (ownerSocket) {
    await req.io.to(ownerSocket).emit('exercise_answered', exercise);
  }
  return res.json({ emited: true });
});

server.listen(2222, function () {
  console.log('App started');
});

// Export the app object. When executing the application local this does nothing. However,
// to port it to AWS Lambda we will create a wrapper around that will load the app from
// this file
module.exports = app;
